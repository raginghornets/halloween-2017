﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuStartup : MonoBehaviour {
    
    public MainMenu mm;
    private GameObject playButton;

    public void Start()
    {
        mm.SetPlayer(null);
        playButton = GameObject.Find("PlayButton");
        playButton.gameObject.SetActive(false);
    }

    public GameObject GetPlayButton()
    {
        return playButton;
    }
}
