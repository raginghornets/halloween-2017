﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {
    [SerializeField]
    private GameObject enemy;

	private void Start () {
        SpawnEnemy(enemy);
	}

    public void SpawnEnemy(GameObject enemy)
    {
        GameObject enemyClone = Instantiate(enemy, transform.position, transform.rotation);
        enemyClone.AddComponent<Enemy>();
    }
}
