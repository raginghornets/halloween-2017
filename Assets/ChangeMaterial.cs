﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterial : MonoBehaviour {

    public Material mat;
    Color origMatColor, newMatColor;

    public void Start()
    {
        origMatColor = new Color(0.875f, 0.875f, 0.875f, 1);
        newMatColor = new Color(origMatColor.r * 0.5f, origMatColor.b * 0.5f, origMatColor.g * 0.5f, 1);
    }

    public void darkenMat()
    {
        if(!onNewMat())
        {
            mat.color = newMatColor;
        }
        else
        {
            mat.color = origMatColor;
        }
    }

    public bool onNewMat()
    {
        return mat.color == newMatColor;
    }
}
